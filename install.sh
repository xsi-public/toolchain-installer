#!/bin/bash

mkdir -p ~/.local;
npm config set prefix ~/.local;

if [ -f ~/.xsi_profile ]; then

	echo "XSI Profile already installed";

else

	echo "Create XSI Profile";
	echo "PATH=~/.local/bin/:$PATH" > ~/.xsi_profile;

	if [ -f ~/.bash_profile ]; then

		echo "link XSI Profile to .bash_profile";
		echo "source .xsi_profile" >> ~/.bash_profile;

	else
		if [ -f ~/.bashrc ]; then

			echo "link XSI Proile to .bashrc";
			echo "source .xsi_profile" >> ~/.bashrc;

		else

			echo "ERROR :: impossible to find bash profile or rcprofile, please create it";
			echo "ex: touch ~/.bash_profile";
			rm .xsi_profile;
			exit 1;
		fi
	fi
fi

GR=$(id -g);
path=$(perl -MCwd -e 'print Cwd::realpath($ARGV[0])' ~/.local/lib/node_modules);
sudo chown -R "$USER":"$GR" "$path";

say -v Victoria "Welcom to XSI";

npm install -g ssh://git@gitlab.com:xsi-tools/toolchain.git;

