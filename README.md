# XSI Toolchain

## Installation

XSI Toolchain have to be installed globally to allow cli commands.
You need to have an already existing XSI Account or a Registration token to validate the installation.

Use the following command to install xsi toolchain environment.


```bash
curl https://gitlab.com/xsi-public/toolchain-installer/raw/master/install.sh > __xsi.sh;  bash __xsi.sh; rm __xsi.sh;
```

After npm install, the installer will pair your computer with the XSI Infrastructure.

## Usage

### Install an existing remote XSI Repository locally 
```bash
xsi install <name>
```

### Create a new remote XSI Repository
```bash
xsi create <name>
```

### Delete the local installation of an existing XSI Repository
```bash
xsi create <name>
```

### Watch an installed XSI repository for development

The toolchain will watch the repository folder. For each changes the system will commit and push the projet and will auto update a docker for development tests.
```bash
xsi watch <name>
```